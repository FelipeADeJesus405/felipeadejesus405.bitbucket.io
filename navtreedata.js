/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Turn Signal Machine", "index.html#sec_turn", null ],
    [ "Motor Driver", "index.html#sec_motor", null ],
    [ "Main Loop", "index.html#sec_main", null ],
    [ "Encoder", "index.html#sec_encoder", null ],
    [ "Controller", "index.html#sec_controller", null ],
    [ "Sensor", "index.html#sec_sensor", null ],
    [ "Sensor Documentation", "sens_page.html", null ],
    [ "Testing", "test_page.html", null ],
    [ "Project", "prop_page.html", [
      [ "Problem", "prop_page.html#sec_pron", null ],
      [ "Requirements", "prop_page.html#sec_req", null ],
      [ "Fabrication", "prop_page.html#sec_fab", null ],
      [ "Risks", "prop_page.html#sec_risks", null ],
      [ "Timeline", "prop_page.html#sec_time", null ],
      [ "Finite State Machines", "prop_page.html#sec_FSM", null ],
      [ "Functionality", "prop_page.html#sec_Func", null ],
      [ "Inner Workings", "prop_page.html#sec_work", null ],
      [ "Testing", "prop_page.html#sec_testing", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';