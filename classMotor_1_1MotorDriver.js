var classMotor_1_1MotorDriver =
[
    [ "__init__", "classMotor_1_1MotorDriver.html#aaacfaace5dcbfd2c4aa283601145a7db", null ],
    [ "disable", "classMotor_1_1MotorDriver.html#a9ad4f746ef0e7c217ce790f7ab9260b3", null ],
    [ "enable", "classMotor_1_1MotorDriver.html#a794de1aa1bfc8ff660c75bf7f4ec8038", null ],
    [ "set_duty", "classMotor_1_1MotorDriver.html#a420e347ab73de4a9851a5da3434ccfbb", null ],
    [ "duty", "classMotor_1_1MotorDriver.html#a5944c862c6ffcc5fc6e59c74e17f4bf8", null ],
    [ "En_pin", "classMotor_1_1MotorDriver.html#ae92e53c3e853cbcaffc275a8d2550f70", null ],
    [ "IN1_pin", "classMotor_1_1MotorDriver.html#a336e963c802260d78feda5ee6240af89", null ],
    [ "IN2_pin", "classMotor_1_1MotorDriver.html#ab381ffdf92b5d73b017aa0e76a593cc7", null ],
    [ "PWM1", "classMotor_1_1MotorDriver.html#a1d422ee4ad062bb7ddc5f08d5d37b343", null ],
    [ "PWM2", "classMotor_1_1MotorDriver.html#a86aa28cebd00cca9d27c96caee8f19a4", null ],
    [ "timer", "classMotor_1_1MotorDriver.html#a93dd3f6608ad30e13b49d9ac609b919d", null ]
];